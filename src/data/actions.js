import spryStore from './spryStore';

export const REQUEST_SPRY_DATA = 'REQUEST_SPRY_DATA';

function requestSpryData() {
  return {
    type: REQUEST_SPRY_DATA
  }
}

export const RECEIVE_SPRY_DATA = 'RECEIVE_SPRY_DATA';

function receiveSpryData(spry) {
  return {
    type: RECEIVE_SPRY_DATA,
    spry
  }
}

export const INDEX_SPRY_DATA = 'INDEX_SPRY_DATA';

export function indexSpryData() {
  return {
    type: INDEX_SPRY_DATA
  }
}


export const CREATE_SPRINT = 'CREATE_SPRINT';

export function createSprint(sprint) {
  return {
    type: CREATE_SPRINT,
    sprint
  }
}

export const CREATE_CONTRIBUTOR = 'CREATE_CONTRIBUTOR';

export function createContributor(contributor) {
  return {
    type: CREATE_CONTRIBUTOR,
    contributor
  }
}

export const CREATE_TASK = 'CREATE_TASK';

export function createTask(task) {
  return {
    type: CREATE_TASK,
    task
  }
}

export const CREATE_PROJECT = 'CREATE_PROJECT';

export function createProject(project) {
  return {
    type: CREATE_PROJECT,
    project
  }
}

export const ADD_TASKS_TO_PROJECT = 'ADD_TASKS_TO_PROJECT';

export function addTasksToProject(tasks, projectId) {
  return {
    type: ADD_TASKS_TO_PROJECT,
    tasks,
    projectId
  }
}

export function fetchSpryData() {
  return dispatch => {
    dispatch(requestSpryData());
    return spryStore.fetchSpryData().then(data => dispatch(receiveSpryData(data)));
  }
}