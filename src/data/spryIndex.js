import { indexSpryData } from './actions';

class SpryIndex {
  constructor(props) {
    this.store = props.store;
    this.store.subscribe(() => this.handleStoreStateChange());
    this.sortedSprints = [];
    this.sortedTasks = [];
    this.sortedProjects = [];
    this.contributors = null;
    this.projects = null;
  }

  handleStoreStateChange() {
    if (this.store.getState().client.dataReceived && !this.store.getState().client.dataIndexed) {
      // Update the indexes
      this.updateIndex();
      this.store.dispatch(indexSpryData());
    }
  }

  getSprint(id) {
    if (this.store.getState().spry.sprints && this.store.getState().spry.sprints[id]) {
      return this.store.getState().spry.sprints[id];
    }
    return null;
  }
  
  getContributor(alias) {
    if (this.store.getState().spry.contributors && this.store.getState().spry.contributors[alias]) {
      return this.store.getState().spry.contributors[alias];
    }
    return null;
  }

  hasSprints() {
    return this.sortedSprints.length > 0;
  }

  hasContributors() {
    return this.contributors !== null;
  }

  hasProjects() {
    return this.projects !== null;
  }

  isEmpty() {
    return !this.hasSprints() || !this.hasContributors(); //|| !this.hasProjects();
  }

  updateIndex() {
    let spry = this.store.getState().spry;

    if (!spry) return;

    if (spry.sprints) {
      let sprints = Object.values(spry.sprints);
      sprints.sort((a, b) => b.startDate - a.startDate);
      this.sortedSprints = sprints;
    }
    
    if (spry.contributors) {
      let contributors = {...spry.contributors};
      this.contributors = contributors;
    }

    if (spry.tasks) {
      let tasks = Object.keys(spry.tasks).sort().reduce((obj, key) => {
        obj[key] = spry.tasks[key];
        return obj;
      }, {});
      this.sortedTasks = tasks;
    }

    if (spry.projects) {
      let projects = Object.keys(spry.projects).sort().reduce((obj, key) => {
        obj[key] = spry.projects[key];
        obj[key].tasks = [];
        return obj;
      }, {});

      const taskKeys = Object.keys(this.sortedTasks);
      for (let i = 0; i < taskKeys.length; i++) {
        let task = this.sortedTasks[taskKeys[i]];
        if (task.project == null) continue;

        projects[task.project].tasks.push(task);
      }

      this.sortedProjects = projects;
    }
  }
}

export default SpryIndex;