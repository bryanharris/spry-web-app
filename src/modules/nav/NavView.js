import React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Menu, Image } from 'semantic-ui-react';

class NavView extends React.Component {
  
  renderEmptyNav() {
    return <Menu pointing className="main-nav" secondary>
             <Menu.Menu>
               <Menu.Item>
                 <Image src='/spry-logo-black.png' height='20px' />
               </Menu.Item>
             </Menu.Menu>
           </Menu>; 
  }
  
  renderNavItems() {
    return (
      <Menu pointing className="main-nav" secondary>
        <Menu.Item active={this.props.activeItem === 'plan'}>
          <Link to="/"><Icon name='tasks' />Sprints</Link>
        </Menu.Item>
        <Menu.Item active={this.props.activeItem === 'tasks'}>
          <Link to="/tasks"><Icon name='columns' />Tasks</Link>
        </Menu.Item>
        <Menu.Item active={this.props.activeItem === 'projects'}>
          <Link to="/projects"><Icon name='lab' />Projects</Link>
        </Menu.Item>
        <Menu.Item active={this.props.activeItem === 'contributors'}>
          <Link to="/contributors"><Icon name='users'/>Contributors</Link>
        </Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item>
            <Image src='/spry-logo-black.png' height='20px' />
          </Menu.Item>
        </Menu.Menu>
       </Menu>
    );
  }

  render() {
    if (this.props.showNavItems !== undefined && this.props.showNavItems === 'false') {
      return this.renderEmptyNav();
    } else {
      return this.renderNavItems();
    }
  }
}

export default NavView;
