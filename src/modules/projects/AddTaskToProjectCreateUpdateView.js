import React from 'react';
import {Form, Dropdown, Checkbox, Table} from 'semantic-ui-react';
import {addTasksToProject, createProject} from '../../data/actions';
import Validator from '../../data/validator';
import CreateUpdateView from '../base/CreateUpdateView';
import { getProjectTypes, getContributorSkills } from '../../config/spryConfig';
import _ from 'lodash';

class AddTaskToProjectCreateUpdateView extends CreateUpdateView {

  constructor(props) {
    super(props);

    this.state = {
      data: this.props.spryIndex.sortedTasks,
      tasks: [],
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event, data) {
    if (data.type === 'checkbox') {
      this.state.tasks.push(data.name);
    } else {
      this.state.tasks[data.name] = data.value.trim();
    }
    this.setState(this.state);
  }

  getInitialState() {
    return { tasks: {} };
  }

  initializePropertiesForNewInstance() {
    // No properties to initialize
  }

  validate() {
    let validator = new Validator();

    return validator.getResult();
  }

  convertDataTypes() {
    // No datatypes to convert
  }

  dispatchCreateAction() {
    this.props.store.dispatch(addTasksToProject(this.state.tasks, this.props.projectId));
  }
  
  getTitle() {
    return this.state.isCreate ? 'Add tasks' : 'Edit tasks'
  }

  renderForm() {
    const { data } = this.state;

    return (
      <Form>
        <Table sortable celled striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell >
                Id
              </Table.HeaderCell>
              <Table.HeaderCell >
                Title
              </Table.HeaderCell>
              <Table.HeaderCell >
                Assignee
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(data, ({ title, assignee }, key) => (
              <Table.Row key={key} selectable>
                <Table.Cell selectable textAlign='center'><Checkbox name={key} onChange={this.onInputChange} /></Table.Cell>
                <Table.Cell>{key}</Table.Cell>
                <Table.Cell>{title}</Table.Cell>
                <Table.Cell>{assignee}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </Form>
    );
  }
}

export default AddTaskToProjectCreateUpdateView;
