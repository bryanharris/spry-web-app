import React from 'react';
import { Form, Dropdown } from 'semantic-ui-react';
import { createProject } from '../../data/actions';
import Validator from '../../data/validator';
import CreateUpdateView from '../base/CreateUpdateView';
import { getProjectTypes, getContributorSkills } from '../../config/spryConfig';

class ProjectCreateUpdateView extends CreateUpdateView {

  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event, data) {
    this.state.project[data.name] = data.value.trim();
    this.setState(this.state);
  }

  getInitialState() {
    return { project: {} };
  }

  initializePropertiesForNewInstance() {
    // No properties to initialize
  }

  validate() {
    let validator = new Validator();

    return validator.getResult();
  }

  convertDataTypes() {
    // No datatypes to convert
  }

  dispatchCreateAction() {
    this.props.store.dispatch(createProject(this.state.project));
  }
  
  getTitle() {
    return this.state.isCreate ? 'Add project' : 'Edit project'
  }

  renderForm() {

    const assigneeOptions = Object.values(this.props.spryIndex.contributors).map((contributor) => {
      return {key: contributor.alias, value: contributor.alias, text: contributor.name};
    }, {});

    const types = getProjectTypes();
    const typeOptions = types.map(type => {
      return { key: type.id, value: type.id, text: type.name, icon: type.icon };
    });

    // Fill these in from the index when they exist.
    const projectOptions = [];
    const sprintOptions = [];

    const { name, leads, description } = this.state.project;

    return (
      <Form>
        <Form.Field required>
          <label>Name</label>
          <Form.Input error={this.state.validationResult.fields.title} name='name' value={name} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field required>
          <label>Description</label>
          <Form.TextArea error={this.state.validationResult.fields.name} name='description' value={description} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field required>
          <label>Assignee</label>
          <Dropdown error={this.state.validationResult.fields.leads} name='leads' value={leads}
                    fluid search selection options={assigneeOptions} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Jira Link</label>
          <Form.Input error={this.state.validationResult.fields.jiraLink} name='jiraLink' onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Type</label>
          <Dropdown name='type' fluid search selection options={typeOptions} onChange={this.onInputChange} />
        </Form.Field>
        <Form.Field>
          <label>Start Date</label>
          <Form.Input error={this.state.validationResult.fields.startDate} name='startDate' onChange={this.onInputChange} />
        </Form.Field>
      </Form>
    );
  }
}

export default ProjectCreateUpdateView;
