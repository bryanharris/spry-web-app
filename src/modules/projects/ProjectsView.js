import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Header, Table, Step } from 'semantic-ui-react';
import _ from 'lodash';

import ProjectCreateUpdateView from './ProjectCreateUpdateView';

class ProjectsView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      column: null,
      data: this.props.spryIndex.sortedProjects,
      direction: null,
    };

    this.props.store.subscribe(() => this.handleStoreStateChange());
  }

  handleStoreStateChange() {
    this.setState({ ...this.state, data: this.props.spryIndex.sortedProjects });
  }

  handleSort = clickedColumn => () => {
    const { column, data, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: 'ascending',
      });

      return
    }

    this.setState({
      data: data.reverse(),
      direction: direction === 'ascending' ? 'descending' : 'ascending',
    })
  };

  render() {
    const { column, data, direction } = this.state;

    return (
      <div>
        <Container>
          <Header>Projects</Header>
          <Table sortable celled striped selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell sorted={column === 'name' ? direction : null} onClick={this.handleSort('id')}>
                  Id
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'age' ? direction : null} onClick={this.handleSort('title')}>
                  Title
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'gender' ? direction : null} onClick={this.handleSort('assignee')}>
                  Assignee
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {_.map(data, ({ name, leads }, key) => (
                <Table.Row key={key}>
                  <Table.Cell selectable><Link to={'/project/' + key}>{key}</Link></Table.Cell>
                  <Table.Cell>{name}</Table.Cell>
                  <Table.Cell>{leads}</Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>

          <Button icon='plus' content='New project' color='blue' onClick={() => {this.createView.open();}} />

          <ProjectCreateUpdateView store={this.props.store} spryIndex={this.props.spryIndex} ref={el => { this.createView = el }} />
        </Container>

      </div>
    );
  }
}

export default ProjectsView;
